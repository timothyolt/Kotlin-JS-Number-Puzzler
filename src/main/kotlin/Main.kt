fun main() {
    println(1 + 1 - 1)
}

fun testDouble(input: Double) {
    println(input)
    println("input: ${input::class.simpleName}")
    val negated = negate(input)
    println("negated return: ${negated::class.simpleName}")
}

fun testInt(input: Int) {
    println(input)
    println("input: ${input::class.simpleName}")
    val negated = negate(input)
    println("negated return: ${negated::class.simpleName}")
}

fun testLong(input: Long) {
    println(input)
    println("input: ${input::class.simpleName}")
    val negated = negate(input)
    println("negated return: ${negated::class.simpleName}")
}

fun <T : Number> negate(number: T): T {
    println("typeof: ${js("typeof number")}")
    val negated = when (number) {
        is Double -> -number
        is Long -> -number
        else -> error("Don't know how we will get here")
    }
    println("number: ${number::class.simpleName}")
    println("negated: ${negated::class.simpleName}")
    return negated as T
}